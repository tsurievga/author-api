package com.barelan.authors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class AuthorsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthorsApplication.class, args);
	}

}

@Configuration
class RestTemplateConfig{

	@Bean
	RestTemplate restTemplate(){
		return new RestTemplate();
	}
}

@Service
class AuthorService {
	private final RestTemplate restTemplate;

	public AuthorService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public String getAuthorDetails(String authorName) throws JsonProcessingException {
		String apiUrl = "https://openlibrary.org/search/authors.json?q=" + authorName; // Replace with the actual API URL
		String response = restTemplate.getForObject(apiUrl, String.class);
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode json = objectMapper.readValue(response, JsonNode.class);
		return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
	}
}

@RestController
class AuthorController {
	private final AuthorService authorService;

	public AuthorController(AuthorService authorService) {
		this.authorService = authorService;
	}

	@GetMapping("/")
	public String getMessage() {
		return "Welcome to DevSecOps";
	}
	@GetMapping("/authors/{authorName}")
	public String getAuthor(@PathVariable String authorName) throws JsonProcessingException {
		return authorService.getAuthorDetails(authorName);
	}
}